import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, Linking, ActivityIndicator} from 'react-native';
import {getRandomFoto} from "../helpers/getRandomUserFoto";
import {Feather} from '@expo/vector-icons';
import {AntDesign} from '@expo/vector-icons';

const Contact = ({navigation, route}) => {
    const [avaURL, setAvaURL] = useState(null);

    useEffect(() => {

        function timeout(seconds) {
            return new Promise(resolve => {
                setTimeout(resolve, seconds * 1000);
            });
        }

        const getRandomPhotoOrMock = async () => {
            let timeLimitedRequest = '';

            setTimeout(async () => {
                timeLimitedRequest = await getRandomFoto();
            }, 0);

            await timeout(2);
            // If no response more than 2 seconds, return mock photo
            if (timeLimitedRequest !== '') return timeLimitedRequest;
            else return 'https://react.semantic-ui.com/images/wireframe/image.png';
        };

        getRandomPhotoOrMock()
            .then(randomPhotoOrMock => {
                if (Boolean(avaURL)) return
                setAvaURL(randomPhotoOrMock);
            })

        navigation.addListener('beforeRemove', (e) => {
            if (Boolean(avaURL)) {
                // If we have upload photo, then we don't need to blocking back button
                navigation.dispatch(e.data.action);
            } else {
                // Prevent default behavior of leaving the screen
                e.preventDefault();
            }
        })
    }, [navigation, avaURL])

    return (
        <View style={{paddingTop: 20,}}>
            <View style={styles.avatar_wrapper}>
                {avaURL ?
                    <View style={styles.shadow}>
                        <Image
                            style={styles.avatar}
                            source={{
                                uri: avaURL,
                            }}
                        />

                    </View>
                    : <ActivityIndicator style={{height: 120}} size="large" color="#00ff00"/>
                }
            </View>
            <View style={styles.row_wrapper}>
                <Text style={styles.label}>Name</Text>
                <Text style={styles.userData}>
                    {route.params.name}
                </Text>
            </View>
            <View style={styles.row_wrapper}>
                <Text style={styles.label}>Phone number</Text>
                <Text style={styles.userData}>
                    {route.params.phone}
                </Text>
            </View>
            <View style={styles.button_row}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        Linking.openURL(`tel:${route.params.phone}`);
                    }}
                >
                    <View style={styles.button_content}>
                        <Feather name="phone-call" size={24} color="white"/>
                        <Text style={styles.button_text}>CALL</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => {
                        navigation.navigate({
                            name: 'Contacts',
                            params: {userId: route.params.id},
                            merge: true,
                        });
                    }}
                >
                    <View style={styles.button_content}>
                        <AntDesign name="delete" size={24} color="white"/>
                        <Text style={styles.button_text}>DELETE</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    avatar_wrapper: {
        marginLeft: 16,
        marginRight: 16,
        justifyContent: "center",
        flexDirection: "row"
    },
    avatar: {
        width: 120,
        height: 120,
        borderRadius: 60,
    },
    shadow: {
        shadowColor: 'black',
        shadowOffset: {
            width: 5,
            height: 5
        },
        shadowRadius: 15,
        elevation: 10,
        backgroundColor: 'white',
        height: 120,
        width: 120,
        borderRadius: 60,
    },
    row_wrapper: {
        marginLeft: 16,
        marginRight: 16,
        paddingTop: 16,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: "rgb(216, 216, 216)"
    },
    userData: {
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        color: '#382A2C',
        fontWeight: "normal",
    },
    label: {
        fontSize: 12,
        fontFamily: "Roboto-Regular",
        opacity: 0.54,
    },
    button_row: {
        paddingTop: 40,
        marginLeft: 16,
        marginRight: 16,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    button: {
        height: 50,
        width: 150,
        backgroundColor: 'grey',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 10
    },
    button_content: {
        color: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button_text: {
        color: 'white',
        marginLeft: 10
    }
});

export default Contact