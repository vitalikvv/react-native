import React, {useContext} from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    TextInput,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';
import {ContextData} from '../context/ContextData';
import { AntDesign } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';


const Contacts = ({ navigation, route }) => {

    const {onChangeUsersSearch, filteredDataUsers, onHandleDeleteUser} = useContext(ContextData)

    React.useEffect(() => {
        if (route.params?.userId) {
            onHandleDeleteUser(route.params?.userId)
        }
    }, [route.params?.userId]);

    const toContact = (name, id, phone) => {
        return (
            navigation.navigate('Contact', {name, id, phone})
        )
    }

    return (
        <View style={{flex: 1, paddingTop: 20}}>
            <Text style={styles.header}>Contacts</Text>
            <View style={styles.searchUsersSection}>
                <View style={styles.searchSVG}>
                    <MaterialIcons name="person-search" size={20} color="black" />
                </View>
                <TextInput
                    style={styles.searchUsersInput}
                    placeholder="Search"
                    onChangeText={(text) => onChangeUsersSearch(text)}
                    underlineColorAndroid="transparent"
                />
            </View>

            <FlatList
                data={filteredDataUsers}
                keyExtractor={({id}) => `${id}`}
                renderItem={({item}) => {
                    return (
                        <TouchableHighlight delayPressIn={80} delayPressOut={500} onPress={()=>toContact(item.name, item.id, item.phone)}>
                            <View style={styles.card}>
                                <View style={styles.ava_container}>
                                    <Text style={styles.ava_text}>{item.name.split(' ').map(function (item) {
                                        return item[0]
                                    }).join('')}</Text>
                                </View>

                                <View style={styles.user_data_wrapper}>
                                    <Text style={styles.title}>{item.name}</Text>
                                    <Text style={styles.email}>{item.email}</Text>
                                </View>
                                <View style={styles.to_contact_container}>
                                    <AntDesign
                                        name="forward"
                                        size={16}
                                        color="#382A2C"
                                        style={styles.to_contact}
                                    />
                                </View>
                            </View>
                        </TouchableHighlight>
                    )
                }}

            />
            <TouchableOpacity
                style={styles.plus_button}
                onPress={()=> navigation.navigate('Add Contact')}
            >
                <Ionicons name="ios-add" size={36} color="white"/>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        fontSize: 16,
        padding: 16,
        fontFamily: "Roboto-Regular",
    },
    searchUsersSection: {
        height: 40,
        margin: 16,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(56, 42, 44, 0.1)",
        borderRadius: 4,
        shadowOffset: {
            width: 5,
            height: 5
        },
        shadowOpacity: 0.15,
        shadowRadius: 10
    },
    searchSVG: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.5
    },
    searchUsersInput: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
    },
    card: {
        margin: 16,
        height: 40,
        borderColor: '#ff7a00',
        borderRadius: 10,
        flex: 1,
        flexDirection: "row",
        alignContent: "flex-start",
    },
    ava_container: {
        width: 40,
        height: 40,
        marginRight: 16,
        backgroundColor: '#6FCF97',
        borderRadius: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    ava_text: {
        fontFamily: "Roboto-Regular",
        fontWeight: "500"
    },
    user_data_wrapper: {
        justifyContent: "space-between"
    },
    title: {
        fontSize: 16,
        fontFamily: "Roboto-Regular",
        color: '#382A2C',
        fontWeight: "normal",
    },
    email: {
        fontSize: 12,
        fontFamily: "Roboto-Regular",
        opacity: 0.54,
    },
    to_contact_container: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end",
    },
    to_contact: {
        marginRight: 10,
        opacity: 0.54,
    },
    plus_button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        width: 64,
        height: 64,
        backgroundColor: '#6fcf97',
        position: 'absolute',
        right: 35,
        bottom: 35,
        shadowOffset: {
            width: 5,
            height: 5
        },
        shadowRadius: 4,
        elevation: 4
    }
});

export default Contacts