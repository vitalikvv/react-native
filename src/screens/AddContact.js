import React, {useContext, useState} from 'react';
import {
    View,
    TextInput,
    StyleSheet,
    Text,
    ScrollView,
    Button,
    Alert
} from 'react-native';
import {ContextData} from "../context/ContextData";

const AddContact = ({navigation, route}) => {

    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const {onHandleAddNewUser} = useContext(ContextData)

    const addNewUser = () => {

        if (!phone.trim() || !email.trim() || !name.trim()) {
            Alert.alert("All field is required");
            return;
        }

        let regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (regEmail.test(email.trim()) === false) {
            Alert.alert("Email is not correct");
            return;
        }

        onHandleAddNewUser({
            name,
            phone,
            email
        })
        navigation.navigate({
            name: 'Contacts',
        });
    }

    return (
        <ScrollView
            style={styles.container}
        >
            <View style={styles.inner}>

                <View style={styles.row_wrapper}>
                    <Text style={styles.label}>Name:</Text>
                    <TextInput
                        style={{height: 40}}
                        placeholder="Type here "
                        onChangeText={text => setName(text)}
                        defaultValue={name}
                    />
                </View>

                <View style={styles.row_wrapper}>
                    <Text style={styles.label}>Phone:</Text>
                    <TextInput
                        style={{height: 40}}
                        placeholder="Type here phone number"
                        onChangeText={text => setPhone(text)}
                        defaultValue={phone}
                        keyboardType={'numeric'}
                    />
                </View>

                <View style={styles.row_wrapper}>
                    <Text style={styles.label}>Email:</Text>
                    <TextInput
                        style={{height: 40}}
                        placeholder="Type here email"
                        onChangeText={text => setEmail(text)}
                        defaultValue={email}
                    />
                </View>

                <View style={styles.btnContainer}>
                    <Button
                        title="Submit"
                        color={'#6fcf97'}
                        onPress={() => addNewUser()}
                    />
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inner: {
        flex: 1,
        justifyContent: "flex-start"
    },
    row_wrapper: {
        marginLeft: 16,
        marginRight: 16,
        paddingTop: 16,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: "rgb(216, 216, 216)"
    },
    btnContainer: {
        backgroundColor: "white",
        marginLeft: 16,
        marginRight: 16,
        paddingTop: 16
    }
});

export default AddContact;
