import {Alert} from 'react-native';

export const getRandomFoto = async () => {
    try {
        const url = 'https://source.unsplash.com/random/800x800/?face';
        const response = await fetch(url);
        return response.url
    } catch (e) {
        Alert.alert('Something wrong')
        return 'https://react.semantic-ui.com/images/wireframe/image.png'
    }
}
