import React, {Component} from 'react';
import {StyleSheet, View, ActivityIndicator, Alert} from 'react-native';
import * as Font from 'expo-font';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Contacts from "./src/screens/Contacts";
import Contact from "./src/screens/Contact";
import {ContextData} from './src/context/ContextData';
import AddContact from "./src/screens/AddContact";
import uuid from 'react-native-uuid';

const Stack = createStackNavigator()

const transitionConfig = {
    animation: 'spring',
    config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
    },
};

let customFonts = {
    'Roboto-Regular': require('./src/assets/fonts/Roboto-Regular.ttf'),
};

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            users: [],
            fontsLoaded: false,
            searchUsersText: "",
            filteredDataUsers: []
        }
    }

    async getUsers() {
        try {
            const usersPromise = await fetch('https://jsonplaceholder.typicode.com/users')
            const users = await usersPromise.json();
            await this.setState({
                isLoading: false,
                users,
            })
            this.filterData(null)
        } catch (error) {
            Alert.alert('Something wrong')
            console.error(error);
        }
    }

    loadFonts() {
        Font.loadAsync(customFonts);
        this.setState({fontsLoaded: true});
    }

    componentDidMount() {
        this.getUsers();
        this.loadFonts();
    }

    handleDelete = (userId) => {
        this.setState({filteredDataUsers: this.state.filteredDataUsers.filter(user => user.id !== userId)})
    }

    handleAddUser = (userData) => {
        let newUser = {};
        Object.assign(newUser,userData,{id: uuid.v4()})
        let users = [newUser, ...this.state.filteredDataUsers];
        this.setState({
            filteredDataUsers: users,
            users
        });
    }

    onChangeUsersSearch = (text) => {
        this.setState({searchUsersText: text})
        this.filterData(text)
    }

    filterData = (searchText) => {
        if (!searchText) {
            this.setState({
                filteredDataUsers: this.state.users
            });
            return
        }
        const array = this.state.users
        let filteredData
        filteredData = array.filter(element => {
            let searchByName = element.name.toLowerCase().includes(searchText.toLowerCase());
            let searchByEmail = element.email.toLowerCase().includes(searchText.toLowerCase());
            return searchByName || searchByEmail
        });

        this.setState({
            filteredDataUsers: filteredData,
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loader}>
                    <ActivityIndicator size="large" color="#00ff00"/>
                </View>
            )
        }
        return (
            <ContextData.Provider
                value={
                    {
                        filteredDataUsers: this.state.filteredDataUsers,
                        onChangeUsersSearch: this.onChangeUsersSearch,
                        onHandleDeleteUser: this.handleDelete,
                        onHandleAddNewUser: this.handleAddUser
                    }
                }
            >
                <NavigationContainer>
                    <Stack.Navigator
                        screenOptions={{
                            cardStyle: {backgroundColor: 'transparent'},
                        }}
                    >
                        <Stack.Screen
                            name="Contacts"
                            component={Contacts}
                            options={{
                                headerShown: false,
                                transitionSpec: {
                                    open: transitionConfig,
                                    close: transitionConfig,
                                },
                            }}
                        />
                        <Stack.Screen
                            name="Contact"
                            component={Contact}
                            options={{
                                transitionSpec: {
                                    open: transitionConfig,
                                    close: transitionConfig,
                                },
                            }}
                        />
                        <Stack.Screen
                            name="Add Contact"
                            component={AddContact}
                            options={{
                                transitionSpec: {
                                    open: transitionConfig,
                                    close: transitionConfig,
                                },
                            }}
                        />
                    </Stack.Navigator>
                </NavigationContainer>
            </ContextData.Provider>
        )
    }
}

const styles = StyleSheet.create({
    loader: {
        flex: 1,
        justifyContent: "center"
    }
});